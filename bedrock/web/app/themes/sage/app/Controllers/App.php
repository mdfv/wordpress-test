<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public function siteDescription()
    {
        return get_bloginfo( 'description', 'display' );
    }

    public function urlCustomLogo()
    {
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $image_custom_logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        $url_custom_logo = (!empty($image_custom_logo[0])) ? $image_custom_logo[0] : '';
        return $url_custom_logo;
    }

    public static function title()
    {
        global $wp_query,$post;
        $id = $wp_query->get_queried_object_id();

        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(
                '%1$s %2$s',
                '<span>' . __( 'Search:', 'sage' ) . '</span>',
                '&ldquo;' . get_search_query() . '&rdquo;'
            );
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title($id);
    }
    
}
