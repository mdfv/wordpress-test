<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FourZeroFour extends Controller
{
    protected $acf = true;

    protected $template = '404';
    
    public function ApiNotFound(){
        try{
            $url = (!empty(get_field('api_url'))) ?  get_field('api_url') : 'https://api.noencontrado.org/v1';
            $path = (!empty(get_field('api_path'))) ?  get_field('api_path') : 'search';
            $method = (!empty(get_field('api_method'))) ? get_field('api_method') : '';
            $data = (!empty(get_field('api_data'))) ?  get_field('api_data') : array();
            $api_key = (!empty(get_field('api_key'))) ? get_field('api_key') : '';

            $this->url = $url.'/'.$path;
            $this->data = $data;
            $this->api_key = $api_key;
            $this->curl = curl_init();

            //METHOD:
            switch ($method){
              case "POST":
              case "PUT":
              default:
                if (!empty($this->data)):
                    $this->url = sprintf("%s?%s", $this->url, http_build_query($this->data));
                endif;
            }

            // OPTIONS:
            curl_setopt($this->curl, CURLOPT_URL, $this->url);
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, array_merge(
              array('Authorization: BEARER '.$this->api_key),
              array('Content-Type: application/json')
            ));
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            // EXECUTE:
            $result = curl_exec($this->curl);
            curl_close($this->curl);

            $isJson = ((is_string($result) && (is_object(json_decode($result)) || is_array(json_decode($result))))) ? true : false;

            if($isJson == false):
                throw new Exception($result);
            endif;

            $array_json = json_decode($result);
            $array_result = (!empty($array_json)) ? (array) $array_json : array();

            return $array_result;

        } catch (Exception $e) {
            return array($e->getMessage());
        }
    }

    public function EnableApiNotFound(){
        return (!empty(get_field('api_enable'))) ? get_field('api_enable') : 0;
    }

    public function LatestNovedades(){
        $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        $exclude_latest_posts = (function_exists('get_field')) ? get_field('exclude_latest_posts') : array();
        $terms_latest_posts = (function_exists('get_field')) ? get_field('categories_latest_posts') : array();

        $number_latest_posts = (function_exists('get_field')) ? get_field('number_latest_posts') : '';
        $number_latest_posts = (!empty($number_latest_posts)) ? $number_latest_posts : 4;

        $args_latest_posts = array(
            'post_type' => 'novedades',
            'posts_per_page' => $number_latest_posts,
            'exclude' => $exclude_latest_posts,
            'post_status' => 'publish',
            'paged' => $paged,
            'tax_query' => array(
                array(
                    'taxonomy' => 'novedades_categorias',
                    'field' => 'term_id',
                    'operator' => 'IN',
                    'terms' => $terms_latest_posts
                )
            ),
            'orderby' => 'date',
            'order' => 'DESC'
        );

        $query_latest_posts = new \WP_Query( $args_latest_posts );

        return $query_latest_posts;
    }

    public function TitleLatestNovedades(){
        $title_latest_posts = (function_exists('get_field')) ? get_field('title_latest_posts') : '';
        $title_latest_posts = (!empty($title_latest_posts)) ? $title_latest_posts : '';
        return $title_latest_posts;
    }
}