@extends('layouts.app')

@section('content')
  @if (!have_posts())

    <div class="alert alert-warning">
      {{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}
    </div>

    @include('partials.content-error404')

  @endif

  @if ( $latest_novedades->have_posts() )
    <div class="module-card">
        @if(!empty($title_latest_novedades))
        <h2>{{ $title_latest_novedades }}</h2>
        @endif

        <div class="row">
        @while($latest_novedades->have_posts()) @php $latest_novedades->the_post() @endphp
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-title">@php the_title() @endphp</h5>

                        @php
                            $terms = get_the_terms(get_the_ID(),'novedades_categorias'); 
                        @endphp

                        @if(!empty($terms))
                            <h6 class="card-subtitle mb-2 text-muted">
                            @foreach ($terms as $term)
                                <a href="{{ get_term_link($term->term_id,'novedades_categorias') }}" target="_top">{{ $term->name }}<a>
                            @endforeach
                            </h6>
                        @endif

                        <div class="card-text">
                            @php the_excerpt() @endphp
                        </div>
                    </div>
                    @if(!empty(get_field('custom_date')))
                    <div class="card-footer text-muted">
                        {{ get_field('custom_date') }}
                    </div>
                    @endif
                </div>
            </div>
        @endwhile
        </div>
    </div>
  @endif

@endsection
