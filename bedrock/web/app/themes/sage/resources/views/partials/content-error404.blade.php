
@php
    $enable_api_not_found = (!empty($enable_api_not_found)) ? $enable_api_not_found : '';
    $enable_api_not_found = (is_404()) ? true : $enable_api_not_found;
@endphp

@if(!empty($api_not_found) && !empty($enable_api_not_found))
    @php 
        $array_api_not_found = (is_array($api_not_found['data']) && (count($api_not_found['data']) > 1)) ? $api_not_found['data'] : array($api_not_found['data']);
    @endphp

    <div id="carouselNoFound" class="carousel slide" data-ride="carousel">
        @if(!empty($api_not_found['status']) && ($api_not_found['status'] == 'notfound'))
            <div class="media module-not-found">
                <div class="media-body bg-light">
                    <h5 class="mt-0">{{ $api_not_found['status'] }}</h5>
                </div>
            </div>
        @else

            @if(is_array($api_not_found['data']) && (count($api_not_found['data']) > 1))
            <ol class="carousel-indicators">
                @foreach ($array_api_not_found as $key => $item)
                @php 
                    $id_not_found = (!empty($item->id)) ? $item->id : '';
                    $class_active = ($key == 0) ? 'active' : '';
                @endphp

                <li data-target="#carouselNoFound" data-slide-to="{{ $key }}" class="{{ $class_active }}"></li>
                @endforeach
            </ol>
            @endif

            <div class="carousel-inner">
                @foreach ($array_api_not_found as $key => $item)
                    @php 
                        $id_not_found = (!empty($item->id)) ? $item->id : '';
                        $active = (!empty($item->active)) ? $item->active : 0;
                        $found = (!empty($item->encontrado)) ? $item->encontrado : '';
                        $firstname = (!empty($item->nombre)) ? $item->nombre : '';
                        $lastname = (!empty($item->apellido)) ? $item->apellido : '';
                        $age = (!empty($item->edad)) ? $item->edad : '';
                        $age_photo = (!empty($item->edad_foto)) ? $item->edad_foto : '';
                        $date_of_disappearance = (!empty($item->fecha_desaparicion)) ? $item->fecha_desaparicion : '';
                        $date_photo = (!empty($item->fecha_foto)) ? $item->fecha_foto : '';
                        $birth_date = (!empty($item->fecha_nacimiento)) ? $item->fecha_nacimiento : '';
                        $recidence = (!empty($item->residencia)) ? $item->residencia : '';

                        $class_active = ($key == 0) ? 'active' : '';
                    @endphp

                    <div class="carousel-item {{ $class_active }}">
                        <div class="media module-not-found">
                            <figure>
                                <img src="https://static.noencontrado.org/img/{{$id_not_found}}.jpg" alt="{{$firstname}} {{$lastname}}">
                            </figure>
                            <div class="media-body bg-light">
                                <h5 class="mt-0">{{$firstname}} {{$lastname}}</h5>
                                <ul>
                                    @if(!empty($age))
                                    <li><strong>Edad actual: </strong> {{ $age }}</li>
                                    @endif

                                    @if(!empty($age_photo))
                                    <li><strong>Edad en la foto: </strong> {{ $age_photo }}</li>
                                    @endif

                                    @if(!empty($recidence))
                                    <li><strong>Localidad: </strong> {{ $recidence }}</li>
                                    @endif

                                    @if(!empty($date_of_disappearance))
                                    <li><strong>Fecha de desaparición: </strong> <date>{{ date_i18n('l, d',strtotime($date_of_disappearance))}} de {{ date_i18n('F',strtotime($date_of_disappearance)) }} de {{ date_i18n('Y',strtotime($date_of_disappearance)) }}</date></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            @if(is_array($api_not_found['data']) && (count($api_not_found['data']) > 1))
            <button class="carousel-control-prev" type="button" data-target="#carouselNoFound" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-target="#carouselNoFound" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </button>
            @endif

        @endif
    </div>
@endif

@php the_content() @endphp
{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}