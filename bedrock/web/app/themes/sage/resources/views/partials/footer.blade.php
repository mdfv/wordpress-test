<footer class="footer content-info">
  @if(has_nav_menu( 'primary_navigation' ) || has_nav_menu( 'footer' ))
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="footer-menu">
							<ul>
							@if(has_nav_menu( 'primary_navigation' ))
                				{!! 
									wp_nav_menu(
										array(
											'container'      => '',
											'depth'          => 2,
											'items_wrap'     => '%3$s',
											'theme_location' => 'primary_navigation',
										)
									);
                				!!}
							@endif

							@if(has_nav_menu( 'footer' ))
                				{!! 
									wp_nav_menu(
										array(
											'container'      => '',
											'depth'          => 2,
											'items_wrap'     => '%3$s',
											'theme_location' => 'footer',
										)
									);
                				!!}
							@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif

  <div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">

						<div class="footer-logo">
							<p>
								<a href="{{ home_url('/') }}" target="_top">
									@if ( !empty( $url_custom_logo ) )
									<img src="{{ esc_url( $url_custom_logo ) }}" alt="{{ bloginfo( 'name' ) }}">
									@else
									  <strong>{{ esc_html( $site_name ) }}</strong>
									@endif
									@if ( $site_description && true === get_theme_mod( 'display_title_and_tagline', true ) )
									<span>{{ $site_description }}</span>
									@endif

									<span>© Copyright {{ date('Y',time()) }}</span>
								</a>
							</p>
						</div>

					</div>
					<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">

						<div class="right">
							@if(is_active_sidebar( 'sidebar-footer' ))
								@php dynamic_sidebar('sidebar-footer') @endphp
							@endif
						</div>

					</div>
				</div>
			</div>
		</div>
</footer>