<header class="header">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top">
      <a class="navbar-brand" href="{{ home_url('/') }}">
        @if ( !empty( $url_custom_logo ) )
        <img src="{{ esc_url( $url_custom_logo ) }}" alt="{{ bloginfo( 'name' ) }}">
        @else
          <span>{{ esc_html( $site_name ) }}</span>
        @endif
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new \App\wp_bootstrap4_navwalker()]) !!}
            @endif
        </ul>
        <form action="{{ home_url('/') }}" method="GET" class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" name="s" placeholder="{{ __('Search', 'sage') }}..." value="{{ get_search_query() }}">
          <button class="btn btn-info my-2 my-sm-0" type="submit">{{ __('Search', 'sage') }}</button>
        </form>
      </div>
    </nav>
  </header>
