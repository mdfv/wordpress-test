@php
  $thumb_id = get_post_thumbnail_id( get_the_ID() );
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', false);
  $thumb_url = (!empty($thumb_url_array[0])) ? $thumb_url_array[0] : '';
@endphp

<article @php post_class() @endphp>
  <div class="card mb-3">
    <div class="row no-gutters">
      @if (!empty($thumb_url))
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <img src="{{ $thumb_url }}" class="card-img-top" alt="{!! get_the_title() !!}">
      </div>

      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
      @else
      <div class="col-md-12">
      @endif

        <div class="card-body">
          <h5 class="card-title"><a href="{{ get_permalink() }}" target="top">{!! get_the_title() !!}</a></h5>
          @if (get_post_type() === 'post' || get_post_type() === 'novedades')
            @include('partials/entry-meta')
          @endif
          <div class="card-text">
            @php the_excerpt() @endphp
          </div>
        </div>
      </div>
    </div>
  </div>

</article>

