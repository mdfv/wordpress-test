@php
  $thumb_id = get_post_thumbnail_id( get_the_ID() );
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', false);
  $thumb_url = (!empty($thumb_url_array[0])) ? $thumb_url_array[0] : '';
@endphp

<article @php post_class() @endphp>
  <figure>
    @if (!empty($thumb_url))
        <img src="{{ $thumb_url }}" class="card-img-top" alt="{!! get_the_title() !!}">
    @endif
  </figure>
  <header>
    @include('partials/entry-meta')
  </header>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>