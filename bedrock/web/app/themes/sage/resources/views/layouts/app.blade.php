<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <main class="main">

      <section class="jumbotron banner">
        <div class="container">
          @if(is_home())
            <h1 class="display-3">Hello, world!</h1>
            <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p>
          @else
            @include('partials.page-header')
          @endif
        </div>
      </section>

      <section class="wrap">
        <div class="container">
          <div class="row">
            @if (App\display_sidebar())
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            @else
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @endif

              <div class="content">
                @yield('content')
              </div>
            </div>
            @if (App\display_sidebar())
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <aside class="sidebar">
                  @include('partials.sidebar')
                </aside>
              </div>
            @endif
          </div>
        </div>
      </section>
    </main>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
